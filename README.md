## Gaisano Promotions

Gaisano Promotions

#### License

MIT


### How to install 


1. GoTo bench folder `frappe-bench` (e.g. cd ~/frappe-bench)
2. input the ff. command in order:

	a. `bench get-app https://gitlab.com/bailabs/gaisano_promotions`

	b. `bench --site [sitename] install-app gaisano_promotions`

	c. `bench migrate`

	d. `bench clear-cache`
	
	e. `bench restart`
