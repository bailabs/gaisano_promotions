// Copyright (c) 2019, bai and contributors
// For license information, please see license.txt

frappe.ui.form.on('Print Barcode', {
	refresh: function(frm) {
		if(cur_frm.doc.status === "Draft") {
            check_role(frappe.session.user, cur_frm,"Barcode Printing Manager")
        } else if (cur_frm.doc.status === "Approved"){
			cur_frm.set_df_property('item_barcode_number', 'read_only', 1);
			cur_frm.set_df_property('item_name', 'read_only', 1);
			cur_frm.set_df_property('item_price', 'read_only', 1);
			cur_frm.set_df_property('quantity', 'read_only', 1);
			cur_frm.set_df_property('promo_production', 'read_only', 1);
			cur_frm.set_df_property('type', 'read_only', 1);
			check_role(frappe.session.user, cur_frm,"Promotions Manager")
		}
		cur_frm.add_custom_button(__("Print"), function(event) {
cur_frm.perm[0].print = 1
        cur_frm.print_doc();
    });
	}
});
cur_frm.cscript.onload = function (frm) {
	frappe.boot.from_cache = 0
}
cur_frm.cscript.promo_production = function (frm) {

	if(cur_frm.doc.promo_production){
		frappe.call({
			method: "frappe.client.get",
			args: {
				doctype: "Promo Production",
				name: cur_frm.doc.promo_production,
			},
			callback: function(r) {


				frappe.call({
					method:"gaisano_promotions.gaisano_promotions.doctype.print_barcode.print_barcode.get_item",
					args: {
						item_name: r.message.promotion_name
					},
					callback: function(rr) {
						cur_frm.doc.item_barcode_number = rr.message[0].barcode_retial
						console.log(rr.message[0].item_price_retail_with_margin)
						console.log(rr.message[0].item_price_retail_with_margin.toString().length)
						console.log(rr.message[0].item_price_retail_with_margin.toString()[rr.message[0].item_price_retail_with_margin.toString().length - 2])
						if (rr.message[0].item_price_retail_with_margin.toString()[rr.message[0].item_price_retail_with_margin.toString().length - 2] === ".") {
							console.log("NISULOOD")
							rr.message[0].item_price_retail_with_margin += "0"
						}
						console.log(rr.message[0].item_price_retail_with_margin.toString())
						cur_frm.doc.item_price = parseFloat(rr.message[0].item_price_retail_with_margin.toString()).toFixed(2)

						cur_frm.doc.item_name = r.message.promotion_name
						cur_frm.doc.quantity = r.message.total_quantity
						cur_frm.refresh()
					}
				})

			}
    	})

	}

}

function check_role(user,cur_frm,role){
	frappe.call({
		method:"gaisano_promotions.create_barcode.check_role",
		args: {
			user: user
		},
		async: false,
		callback: function(r) {
			console.log(r)
			console.log("ITS OKAY")
			for(var i=0;i<r.message.length;i+=1){
				if(r.message[i].includes(role)){
						cur_frm.add_custom_button(__(role === "Barcode Printing Manager" ? "Approve" : role === "Promotions Manager" ?  "Print New":""), function() {
							if(cur_frm.doc.status === "Draft"){
								frappe.confirm(
									'Are you sure you want to approve barcode printing?',
									function(){
										confirmation("Barcode Printing Manager",cur_frm,0)
									},
									function(){
										confirmation("Promotions Manager",cur_frm, 0)
									}
								)

							} else{
								cur_frm.perm[0].print = 1
                    		confirmation("Promotions Manager",cur_frm,1)
							}
						});

				}
			}
		}
	})
}

function confirmation(role,cur_frm,bol){
	cur_frm.doc.status = role === "Barcode Printing Manager" ? "Approved" : role === "Promotions Manager" ?  "Draft":""
	cur_frm.set_df_property('item_barcode_number', 'read_only', role === "Barcode Printing Manager" ? 1 : role === "Promotions Manager" ?  0:0);
	cur_frm.set_df_property('item_name', 'read_only', role === "Barcode Printing Manager" ? 1 : role === "Promotions Manager" ?  0:0);
	cur_frm.set_df_property('item_price', 'read_only', role === "Barcode Printing Manager" ? 1 : role === "Promotions Manager" ?  0:0);
	cur_frm.set_df_property('promo_production', 'read_only', role === "Barcode Printing Manager" ? 1 : role === "Promotions Manager" ?  0:0);
	cur_frm.set_df_property('quantity', 'read_only', role === "Barcode Printing Manager" ? 1 : role === "Promotions Manager" ?  0:0);
	cur_frm.set_df_property('type', 'read_only', role === "Barcode Printing Manager" ? 1 : role === "Promotions Manager" ?  0:0);
	cur_frm.refresh()
	console.log(frappe)
		frappe.ui.toolbar.clear_cache();
		show_alert("The page will reload")

	cur_frm.save()
}