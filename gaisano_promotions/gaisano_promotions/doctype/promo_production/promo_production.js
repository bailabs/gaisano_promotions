// Copyright (c) 2018, bai and contributors
// For license information, please see license.txt
cur_frm.cscript.refresh = function (frm) {
    if(!cur_frm.doc.stock_entry_name && cur_frm.doc.docstatus === 1) {
        cur_frm.add_custom_button("Create Stock Entry", function (frm) {
            frappe.call({
                method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.recreate_stock_entry",
                args: {"name": cur_frm.doc.name},
                callback: function (r) {
                    if (r.message) {
                        frappe.ui.toolbar.clear_cache()
                    }
                }
            })
        })
    }
     cur_frm.fields_dict['promo_name'].get_query = function(doc) {
			return {
				filters: [
					["allocation_budget", ">", 0]
				]
			}
		}
}
cur_frm.cscript.validate = function (frm) {
    if(!cur_frm.docname.includes("New Promo Production")){
            compute_total_costs(cur_frm)
    }
}
cur_frm.cscript.after_save = function (frm) {
    console.log("NAME")
    console.log(cur_frm.docname)
    console.log(cur_frm.doc.total_costs)
    console.log(cur_frm.doc.srp)
    console.log(cur_frm.doc.subtotal_op)
    frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.update_promo_prod",
            args: {
                "name": cur_frm.docname,
                "total_costs": cur_frm.doc.total_costs,
                "srp": cur_frm.doc.srp,
                "suptotal_op": cur_frm.doc.subtotal_op,
            },
            callback: function (r) {}
    })
}
cur_frm.cscript.promo_name = function (frm, cdt, cdn) {
	if(frm.promo_name) {
        frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_required_items",
            args: {
                promo_series: frm.promo_name
            },

            callback: function (r) {
                console.log(r.message)
				var x = 0
				var xx = 0
                   cur_frm.doc.available_bundle = r.message[1][0].allocation_budget
                   cur_frm.doc.total_quantity = r.message[1][0].allocation_budget
                   cur_frm.doc.promotion_name = r.message[1][0].new_item_name
                   cur_frm.doc.debit_memo = r.message[1][0].debit_memo
                   cur_frm.doc.branch_code = r.message[1][0].branch_code
                   cur_frm.doc.purchase_order = r.message[1][0].purchase_order
                   cur_frm.doc.receiving_report = r.message[1][0].receiving_report
                   cur_frm.doc.purchase_invoice = r.message[1][0].purchase_invoice

                cur_frm.refresh_field("purchase_invoice")
                cur_frm.refresh_field("receiving_report")
                cur_frm.refresh_field("purchase_order")
                cur_frm.refresh_field("debit_memo")
                cur_frm.refresh_field("promotion_name")
                cur_frm.refresh_field("total_quantity")
                cur_frm.refresh_field("available_bundle")
                cur_frm.refresh_field("branch_code")
                if(frm.required_items !== undefined ) {
                    var requiredItemsLength = frm.required_items.length
                    while (x < requiredItemsLength) {
						cur_frm.get_field("required_items").grid.grid_rows[0].remove()
                    	x+=1;
                    }

                }
                console.log(frm.productions_costs)
                if(frm.productions_costs !== undefined){
                    var productionCosts = frm.productions_costs.length
                     while (xx < productionCosts) {
						cur_frm.get_field("productions_costs").grid.grid_rows[0].remove()
                    	xx+=1;
                    }
                     for(var xv=0;xv<frm.productions_costs.length;xv+=1){
                        frm.productions_costs[xv].subtotal_cost = (parseFloat(frm.productions_costs[xv].operating_cost) * parseFloat(frm.total_quantity));
                     }
                }
                if(r.message[0].length > 0) {
                    for (var v = 0; v < r.message[0].length; v++) {
                        if(r.message[0][v].free_item){
                            var new_row = cur_frm.add_child("required_items")
                            var df = frappe.meta.get_docfield("Promo Production Item", "item_name", cur_frm.doc.name);
                            df.options = r.message[4]
                            new_row.item_name = r.message[0][v].description
                            new_row.required_qty =  (parseFloat(frm.total_quantity) * parseFloat(r.message[0][v].quantity_pitem))/parseFloat(r.message[1][0].pcs_in_pack).toString()
                            new_row.source_warehouse = r.message[2][0].name
                            new_row.free_item = r.message[0][v].free_item
                            new_row.item_code = r.message[0][v].promo_item

                        } else {
                            var new_row = cur_frm.add_child("required_items")
                            var df = frappe.meta.get_docfield("Promo Production Item", "item_name", cur_frm.doc.name);
                            df.options = r.message[4]
                            new_row.item_name = r.message[0][v].description
                            new_row.required_qty =  (parseFloat(r.message[0][v].quantity_pitem) * parseFloat(frm.total_quantity)).toString()
                            new_row.source_warehouse = r.message[2][0].name
                            new_row.free_item = r.message[0][v].free_item
                            new_row.item_code = r.message[0][v].promo_item

                        }
                        if(v < r.message[5].length){
                            var new_row = cur_frm.add_child("required_items")
                            var df = frappe.meta.get_docfield("Promo Production Item", "item_name", cur_frm.doc.name);
                            df.options = r.message[4]
                            new_row.item_name = r.message[5][v].other_name
                            new_row.required_qty =  (parseFloat(frm.total_quantity) * parseFloat(r.message[5][v].other_qty))/parseFloat(r.message[1][0].pcs_in_pack).toString()
                            new_row.source_warehouse = r.message[2][0].name
                            new_row.item_code = r.message[2][0].promo_item
                            new_row.free_item = 1
                        }

                    }

                    for (var vv = 0; vv < r.message[3].length; vv++) {
                        if((r.message[3][vv].name).toLowerCase() !== "sealer"){
                             var new_row = cur_frm.add_child("productions_costs")
                        new_row.operation = r.message[3][vv].name;
                        new_row.operating_cost = r.message[3][vv].price;
                        new_row.subtotal_cost = (parseFloat(r.message[3][vv].price) * parseFloat(frm.total_quantity));

                        }

                    }
                }

                refresh_field("required_items")
                refresh_field("productions_costs")
                compute_total_costs(cur_frm)
            }

        });


    }
}
cur_frm.cscript.total_quantity = function (frm) {
console.log("PROMO SERIES")
	if(frm.promo_name && frm.total_quantity) {
        frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_required_items",
            args: {
                promo_series: frm.promo_name
            },

            callback: function (r) {
                console.log(r.message)
				var x = 0
				var xx = 0
                 if(frm.required_items !== undefined ) {
                     var requiredItemsLength = frm.required_items.length

                    while (x < requiredItemsLength) {
						cur_frm.get_field("required_items").grid.grid_rows[0].remove()
                    	x+=1;
                    }

                }
                if(frm.productions_costs !== undefined){
                                        var productionCosts = frm.productions_costs.length

                    for(var xv=0;xv<productionCosts;xv+=1){
                 frm.productions_costs[xv].subtotal_cost = (parseFloat(frm.productions_costs[xv].operating_cost) * parseFloat(frm.total_quantity));
                }
                }
                if(r.message[0].length > 0) {
                    for (var v = 0; v < r.message[0].length; v++) {
                        if(r.message[0][v].free_item){
                            var new_row = cur_frm.add_child("required_items")
                            var df = frappe.meta.get_docfield("Promo Production Item", "item_name", cur_frm.doc.name);
                            df.options = r.message[4]
                            new_row.item_name = r.message[0][v].description
                            new_row.required_qty =  (parseFloat(frm.total_quantity) * parseFloat(r.message[0][v].quantity_pitem))/parseFloat(r.message[1][0].pcs_in_pack).toString()
                            new_row.source_warehouse = r.message[2][0].name
                                                                                    new_row.free_item = r.message[0][v].free_item


                        } else {
                            var new_row = cur_frm.add_child("required_items")
                        var df = frappe.meta.get_docfield("Promo Production Item", "item_name", cur_frm.doc.name);
                        df.options = r.message[4]
                        new_row.item_name = r.message[0][v].description
                        new_row.required_qty =  (parseFloat(r.message[0][v].quantity_pitem) * parseFloat(frm.total_quantity)).toString()
                        new_row.source_warehouse = r.message[2][0].name
                                                                                   new_row.free_item = r.message[0][v].free_item


                        }

                    }

                }

                refresh_field("required_items")
                refresh_field("productions_costs")
compute_total_costs(cur_frm)
            }

        });

    }
}
cur_frm.cscript.onload = function (frm) {
    if(!cur_frm.doc.purchase_invoice || !cur_frm.doc.receiving_report || !cur_frm.doc.purchase_order){
			cur_frm.fields_dict['sb11'].collapse();
		}
    if(cur_frm.doc.edit_srp){
        cur_frm.set_df_property('srp', 'read_only', !cur_frm.doc.edit_srp);
		cur_frm.refresh_field("srp");
    }
    cur_frm.fields_dict['promo_name'].get_query = function(doc) {
	return {
		filters: {
			"docstatus": 1
		}
	}
}
    var xx=0
    if(frm.name.includes("New Promo Production")){
         if(frm.productions_costs !== undefined){
                                                     var productionCosts = frm.productions_costs.length

          while (xx < productionCosts) {
						cur_frm.get_field("productions_costs").grid.grid_rows[0].remove()
                    	xx+=1;
                    }
         }
    if(frm.productions_costs === undefined) {
             console.log("COOOOSTS")
         frappe.call({
             method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_all",
             args: {},
             callback: function (r) {
                 console.log("R MESSAGE")
                 console.log(r)
                for (var v = 0; v < r.message.length; v++) {
                    if((r.message[v].operation).toLowerCase() !== "sealer"){
                         var new_row = cur_frm.add_child("productions_costs")
                    new_row.operation = r.message[v].operation;
                    new_row.operating_cost = r.message[v].price;
                    } else {
                        cur_frm.set_value("sealer", r.message[v].price)
                    }

                }
                	refresh_field("sealer")
                	refresh_field("productions_costs")
                 cur_frm.set_value("cellophane_price","0.00")
                 cur_frm.set_value("cellophane_subtotal","0.00")
                 cur_frm.set_value("scotchtape_price","0.00")
                 cur_frm.set_value("scotchtape_subtotal","0.00")
                 cur_frm.set_value("sealer_subtotal","0.00")
                 cur_frm.refresh_field("sealer_subtotal")
                 cur_frm.refresh_field("scotchtape_price")
                 cur_frm.refresh_field("scotchtape_subtotal")
                 cur_frm.refresh_field("cellophane_price")
                 cur_frm.refresh_field("cellophane_subtotal")

            }
         })

    }
    }
}

cur_frm.cscript.operating_cost = function (frm) {
    for(var x=0;x<frm.productions_costs.length;x+=1){
        frm.productions_costs[x].subtotal_cost = (parseFloat(frm.productions_costs[x].operating_cost) * parseFloat(frm.total_quantity));

    }
            	refresh_field("productions_costs")
}


cur_frm.cscript.cellophane = function (frm) {
    var test = 0;
    if (cur_frm.doc.cellophane) {

        frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_cellophane_price",
            args: {
                "cellophane": cur_frm.doc.cellophane
            },
            callback: function (r) {
                console.log(r.message)
                if (r.message.length > 0) {
                    cur_frm.set_value("cellophane_price",r.message[0].price)
                    cur_frm.refresh_field("cellophane_price")
                    if(cur_frm.doc.c_quantity){
                        cur_frm.set_value("cellophane_subtotal",cur_frm.doc.c_quantity * r.message[0].price)
                    cur_frm.refresh_field("cellophane_subtotal")
                    }
                }
            }

        });
    } else {
        cur_frm.set_value("cellophane_price","0.00")
        cur_frm.set_value("cellophane_subtotal","0.00")
        cur_frm.refresh_field("cellophane_price")
        cur_frm.refresh_field("cellophane_subtotal")
    }

};

cur_frm.cscript.scotch_tape = function (frm) {
    var test = 0;
    if (cur_frm.doc.scotch_tape) {
        frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_scotch_tape_price",
            args: {
                "scotch_tape": cur_frm.doc.scotch_tape
            },
            callback: function (r) {
                console.log(r.message)
                if (r.message.length > 0) {
                    cur_frm.set_value("scotchtape_price",r.message[0].price)
                    cur_frm.refresh_field("scotchtape_price")
                    if(cur_frm.doc.st_quantity){
                        cur_frm.set_value("scotchtape_subtotal",cur_frm.doc.st_quantity * r.message[0].price)
                        cur_frm.refresh_field("scotchtape_subtotal")
                    }
                }
            }

        });
    } else {
        cur_frm.set_value("scotchtape_price","0.00")
        cur_frm.set_value("scotchtape_subtotal","0.00")
        cur_frm.refresh_field("scotchtape_price")
        cur_frm.refresh_field("scotchtape_subtotal")
    }

};
cur_frm.cscript.c_quantity = function (frm) {
    if (cur_frm.doc.c_quantity && cur_frm.doc.cellophane_price) {
        cur_frm.set_value("cellophane_subtotal",cur_frm.doc.c_quantity * cur_frm.doc.cellophane_price)
        cur_frm.refresh_field("cellophane_subtotal")

    }

};
cur_frm.cscript.st_quantity = function (frm) {
    if (cur_frm.doc.st_quantity && cur_frm.doc.scotchtape_price) {
        cur_frm.set_value("scotchtape_subtotal",cur_frm.doc.st_quantity * cur_frm.doc.scotchtape_price)
        cur_frm.refresh_field("scotchtape_subtotal")

    }

};
cur_frm.cscript.edit_srp = function (frm,cdt,cdn) {
	cur_frm.set_df_property('srp', 'read_only', !cur_frm.doc.edit_srp);
		cur_frm.refresh_field("srp");
}
cur_frm.cscript.hours = function (frm) {
    if (cur_frm.doc.sealer && cur_frm.doc.hours) {
        cur_frm.set_value("sealer_subtotal",cur_frm.doc.sealer * cur_frm.doc.hours)
        cur_frm.refresh_field("sealer_subtotal")
    } else if (cur_frm.doc.sealer && !cur_frm.doc.hours){
        cur_frm.set_value("sealer_subtotal","0.00")
        cur_frm.refresh_field("sealer_subtotal")
    }
};
function compute_total_costs(cur_frm){
    var edit_srp = cur_frm.doc.edit_srp !== undefined ? cur_frm.doc.edit_srp : false
    frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.compute_total_costs",
           args: {
                "total_quantity": cur_frm.doc.total_quantity,
               "promo_name":cur_frm.doc.promo_name,
               "purchase_order":cur_frm.doc.purchase_order ? cur_frm.doc.purchase_order : "",
               "receiving_report": cur_frm.doc.receiving_report ? cur_frm.doc.receiving_report : "",
               "purchase_invoice":cur_frm.doc.purchase_invoice ? cur_frm.doc.purchase_invoice : "",
               "cellophane_subtotal":cur_frm.doc.cellophane_subtotal > 0 ? cur_frm.doc.cellophane_subtotal : 0,
               "scotchtape_subtotal":cur_frm.doc.scotchtape_subtotal > 0 ? cur_frm.doc.scotchtape_subtotal : 0,
               "sealer_subtotal":cur_frm.doc.sealer_subtotal > 0 ? cur_frm.doc.sealer_subtotal : 0,
               "production_costs_table":cur_frm.doc.productions_costs,
               "edit_srp": cur_frm.doc.edit_srp !== undefined ? cur_frm.doc.edit_srp.toString() : "false",
           },
        async: false,
            callback: function (r) {
                if(cur_frm.doc.total_quantity > 0 && !edit_srp){
                    console.log("AJSHDLIQWELHJASADNM<AS")
                    cur_frm.doc.total_costs = r.message[0]
                    cur_frm.doc.srp = r.message[1]
                    cur_frm.doc.subtotal_op = compute_prod_costs(cur_frm)
                    refresh_field("subtotal_op")
                    refresh_field("total_costs")
                    refresh_field("srp")
                } else {
                    cur_frm.doc.total_costs = r.message[0]
                    cur_frm.doc.subtotal_op = compute_prod_costs(cur_frm)
                    refresh_field("subtotal_op")
                    refresh_field("total_costs")
                }

            }
        })
    frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_warehouse",
           args: {},
        async: false,
            callback: function (r) {
                if(r.message.length > 0){
                    cur_frm.doc.target_warehouse = r.message[0].name
                    refresh_field("target_warehouse")
                }
            }
        })

    if(cur_frm.docname.includes("New Promo Production")){
        cur_frm.save()
    }

}

function compute_prod_costs(cur_frm){
    var total = 0
    for (var x=0; x < cur_frm.doc.productions_costs.length ; x+=1) {
        total += parseFloat(cur_frm.doc.productions_costs[x].operating_cost) * parseFloat(cur_frm.doc.total_quantity)
    }
    return total
}
