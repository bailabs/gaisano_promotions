// Copyright (c) 2018, bai and contributors
// For license information, please see license.txt

frappe.ui.form.on('Promotions Budget', {
	refresh: function(frm) {
		if (cur_frm.doc.docstatus === 1){
			frm.add_custom_button(__('Promotions'), function() {
				/*frappe.call({
					method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.make_promo_production",
					args: {
						"series": cur_frm.doc.name
					},

					callback: function(r){
						console.log(r.message.name);
						frappe.set_route("Form", "Promo Production", r.message.name);
					}
				});	*/

				frappe.route_options = {
					promotions_budget: cur_frm.doc.name
				};

				frappe.new_doc('Promotions');




			}, __('Make'));
			frm.page.set_inner_btn_group_as_primary(__("Make"));
		}

	}
});

cur_frm.cscript.total_budget=function (frm) {
	var total_budge = 0;
	if (cur_frm.doc.total_budget > 0){
		total_budg = cur_frm.doc.total_budget;

		cur_frm.doc.budget_balance = total_budg;
		cur_frm.refresh();
	}
};
cur_frm.cscript.promotions_calculation=function (frm) {
	if(cur_frm.doc.promotions_calculation){
		frappe.call({
             method: "frappe.client.get",
             args: {
                 doctype: "Promotions Calculation",
                 name: cur_frm.doc.promotions_calculation,
             },
             callback: function(r) {
                 if(r.message) {
					 cur_frm.doc.supplier = r.message.supplier
					 cur_frm.doc.branch_code = r.message.branch
					 cur_frm.doc.total_budget = r.message.total_costs
					 cur_frm.doc.budget_balance = r.message.total_costs
					 cur_frm.doc.purchase_order = r.message.purchase_order
					 cur_frm.doc.receiving_report = r.message.receiving_report
					 cur_frm.doc.purchase_invoice = r.message.purchase_invoice
					 cur_frm.refresh_field("purchase_invoice")
					 cur_frm.refresh_field("receiving_report")
					 cur_frm.refresh_field("purchase_order")
					 cur_frm.refresh_field("supplier")
					 cur_frm.refresh_field("branch_code")
					 cur_frm.refresh_field("total_budget")
					 cur_frm.refresh_field("budget_balance")
                     console.log(r.message)
                 }
             }
         });
	}
};

