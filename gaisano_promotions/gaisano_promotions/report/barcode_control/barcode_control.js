// Copyright (c) 2016, bai and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Barcode Control"] = {
	"filters": [
		{
            "fieldname": "promotions_budget",
            "label": __("Promotions Budget"),
            "fieldtype": "Link",
            "options": "Promotions Budget",
            "get_query": function() {
                return {
                    "doctype": "Promotions Budget",
                    "filters": {
                        "docstatus": 1,
                    }
                }
            },
			"reqd": 1
        },
        {
            "fieldname": "promotions_scheme",
            "label": __("Promotions Scheme"),
            "fieldtype": "Select",
            "options": "Buy x Take x \n Rebates \n Ka Negosyo \n Suki Points",
			"reqd": 1
        },
        {
            "fieldname": "date",
            "label": __("Date"),
            "fieldtype": "Date",
			"reqd": 1
        }
	]
}
