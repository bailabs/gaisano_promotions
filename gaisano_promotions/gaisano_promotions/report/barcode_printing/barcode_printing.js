// Copyright (c) 2016, bai and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Barcode Printing"] = {
	"filters": [
		{
            "fieldname": "promo_production",
            "label": __("Promo Production"),
            "fieldtype": "Link",
            "options": "Promo Production",
            "get_query": function() {
                return {
                    "doctype": "Promo Production",
                    "filters": {
                        "docstatus": 1,
                    }
                }
            },
			"reqd": 1
        }
	]
}
