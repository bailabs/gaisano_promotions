# Copyright (c) 2013, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	print("TEEEEEEEEEEEEEEEEEEEEEEEEESTTT")
	print(filters.get("window"))
	columns = [
		{"label": "Promo Series", 'width': 200, "fieldname": "promo_series"},
		{"label": "Promo Item", 'width': 200, "fieldname": "promo_item"},
		{"label": "Promotions Budget", 'width': 150, "fieldname": "promotions_budget"},
		{"label": "Barcode", 'width': 150, "fieldname": "barcode_retail"},
		{"label": "Total Quantity to produce", 'width': 200, "fieldname": "total_quantity"},
		{"label": "Barcode Image", 'width': 200, "fieldname": "barcode"}
	]

	promo_production_data = frappe.db.sql(""" SELECT * FROM `tabPromo Production` where name=%s """,(filters.get("promo_production")),as_dict=True)

	if len(promo_production_data) > 0:
		item_data = frappe.db.sql(""" SELECT barcode_retial FROM tabItem WHERE item_name_dummy=%s """,(promo_production_data[0].promotion_name),as_dict=True)
		barcode_for_bundle = frappe.db.sql(""" SELECT * FROM `tabBarcode Label` WHERE name=%s """,(item_data[0].barcode_retial),as_dict=True)
		for i in promo_production_data:
			data.append({
				'promo_series': i.promo_name or "",
				'promo_item': i.promotion_name or "",
				'promotions_budget': i.debit_memo or "",
				'barcode_retail':item_data[0].barcode_retial or "",
				'total_quantity': i.total_quantity or "",
				"barcode": str(barcode_for_bundle[0].image)
			})
	return columns, data