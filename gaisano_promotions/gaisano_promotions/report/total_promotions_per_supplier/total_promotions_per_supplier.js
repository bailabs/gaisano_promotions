// Copyright (c) 2016, bai and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Total Promotions per Supplier"] = {
	"filters": [
		{
            "fieldname": "supplier",
            "label": __("Supplier"),
            "fieldtype": "Link",
            "options": "Supplier"
        },
		{
            "fieldname": "start_date",
            "label": __("Start Date"),
            "fieldtype": "Date"
        },
		{
            "fieldname": "end_date",
            "label": __("End Date"),
            "fieldtype": "Date"
        }
	]
}
