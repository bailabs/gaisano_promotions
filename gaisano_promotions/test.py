import frappe
from gaisano.events import create_barcode

@frappe.whitelist()
def test():
    material_receipt_doc = frappe.get_doc({
        "doctype": "Role",
        "role_name": "Test Role 1",
    })
    try:
        material_receipt_doc.insert(ignore_permissions=True)

        material_receipt_doc.submit()
        print("NAAAAAAAAAAAAAAAAMMME")
        print(material_receipt_doc.name)
    except Exception:
        print(frappe.get_traceback())

    frappe.db.sql(""" DELETE FROM `tabRole` WHERE name= %s""",material_receipt_doc.name)



@frappe.whitelist()
def test_save():
    data = frappe.get_doc("Promo Production", "PROD-MALL-000503")
    data.save()

@frappe.whitelist()
def change_item_cost():
    promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions`""", as_dict=True)
    for i in promotions_data:
        if not i.use_existing_bundle:
            smallest_item_cost = ""
            for ii in i.item_table:
                item_data_for_item_cost = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE name=%s """, (ii.promo_item), as_dict=True)
                if smallest_item_cost:
                    if float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " ")) < smallest_item_cost:
                        smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
                else:
                    smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))

            item_cost = float(i.srp_of_bundle) / float(float(smallest_item_cost)/100 + 1)
            frappe.db.sql(""" UPDATE `tabItem` set item_cost=%s WHERE item_cost=%s and type=%s and item_name_dummy=%s and item_price_retail_with_margin=%s""",(i.srp_of_bundle,item_cost,"Company Bundling", i.new_item_name, i.srp_of_bundle))


def check():
    smallest_item_cost = ""
    item_data_for_item_cost = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE name=%s """,("ITEM-076278"), as_dict=True)
    if smallest_item_cost:
        if float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " ")) < float(smallest_item_cost.replace("%", " ")):
            smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
    else:
        smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
    print("SMALLEST")
    print(smallest_item_cost)

def create_stock_entry(prod_name):
    promo_prod = frappe.db.sql(""" SELECT * FROM `tabPromo Production` WHERE name=%s """,(prod_name), as_dict=True)
    warehouse_default_promo = frappe.db.sql(""" SELECT name FROM `tabWarehouse` WHERE name LIKE %s """, ("Promo%"),
                                            as_dict=True)

    item_name_in_promotions = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """, (promo_prod[0].promo_name),
                                            as_dict=True)
    item_code_new_item = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE item_name_dummy=%s """,
                                       (item_name_in_promotions[0].new_item_name), as_dict=True)
    promo_budget_branch_code = frappe.db.sql(""" SELECT branch_code FROM `tabPromotions Budget` WHERE name=%s """,
                                             (promo_prod[0].debit_memo), as_dict=True)
    material_receipt_doc = frappe.get_doc({
        "doctype": "Stock Entry",
        "type": "Stock Transfer",
        "branch": promo_budget_branch_code[0].branch_code,
        "naming_series": "PROMO-." + promo_prod[0].branch_code + ".-.YY.MM.DD",
    })
    material_receipt_doc.append("items", {
        "s_warehouse": warehouse_default_promo[0].name,
        "t_warehouse": promo_prod[0].target_warehouse,
        "item_code": item_code_new_item[0].item_code,
        "qty_dummy": float(promo_prod[0].total_quantity),
        "qty": float(promo_prod[0].total_quantity),
        "uom": "Case",
    })
    try:
        material_receipt_doc.insert(ignore_permissions=True)
        frappe.db.sql(""" UPDATE `tabPromo Production` SET stock_entry_name=%s  WHERE name=%s""",
                      (material_receipt_doc.name, promo_prod[0].name))
        material_receipt_doc.submit()
    except Exception:
        print(frappe.get_traceback())